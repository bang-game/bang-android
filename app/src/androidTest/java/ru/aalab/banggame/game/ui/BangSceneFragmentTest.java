package ru.aalab.banggame.game.ui;

import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import ru.aalab.banggame.R;
import ru.aalab.banggame.utils.SuperFragmentTest;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@RunWith(AndroidJUnit4.class)
public class BangSceneFragmentTest extends SuperFragmentTest {

    @Test
    public void onPlayerOneBgClick() {
        BangListener mockListener = mock(BangListener.class);

        launch(BangSceneFragment.newScene(mockListener));
        onView(withId(R.id.fragment_bang_scene_player_1_bg)).perform(click());

        captureScreenshot();
        verify(mockListener).bangPlayerOne(any());
    }

    @Test
    public void onPlayerTwoBgClick() {
        BangListener mockListener = mock(BangListener.class);

        launch(BangSceneFragment.newScene(mockListener));
        onView(withId(R.id.fragment_bang_scene_player_1_bg)).perform(click());

        captureScreenshot();
        verify(mockListener).bangPlayerOne(any());
    }
}