package ru.aalab.banggame.utils;


import android.support.test.rule.ActivityTestRule;
import android.support.v4.app.Fragment;

import org.junit.Rule;

import ru.aalab.banggame.SuperIntegrationTest;


abstract public class SuperFragmentTest extends SuperIntegrationTest {

    @Rule
    public ActivityTestRule<SingleFragmentActivityForTest> activityRule = new ActivityTestRule<>(SingleFragmentActivityForTest.class, false, false);



    protected <T extends Fragment> T launch(T signInEnterPhoneFragment) {
        activityRule.launchActivity(null);
        SingleFragmentActivityForTest activity = activityRule.getActivity();
        activity.showFragment(signInEnterPhoneFragment);
        return signInEnterPhoneFragment;
    }
}
