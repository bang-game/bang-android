package ru.aalab.banggame;

import android.app.Activity;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.core.internal.deps.guava.collect.Iterables;
import android.support.test.rule.GrantPermissionRule;
import android.support.test.runner.lifecycle.ActivityLifecycleMonitorRegistry;
import android.support.test.runner.lifecycle.Stage;

import com.jraska.falcon.FalconSpoonRule;
import com.metova.cappuccino.animations.SystemAnimations;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;


import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.support.test.InstrumentationRegistry.getInstrumentation;

/**
 *
 */

abstract public class SuperIntegrationTest {


    protected Context instrumentationCtx = InstrumentationRegistry.getTargetContext();


    @Rule
    public final FalconSpoonRule falconSpoonRule = new FalconSpoonRule();

    @Rule
    public final GrantPermissionRule mGrantPermissionRule = GrantPermissionRule.grant(READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE);


    @BeforeClass
    public static void disableAnimations() {
        SystemAnimations.disableAll(InstrumentationRegistry.getContext());
    }

    @AfterClass
    public static void enableAnimations() {
        SystemAnimations.enableAll(InstrumentationRegistry.getContext());
    }

    protected void captureScreenshot() {
        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        String methodName = stackTraceElements[3].getMethodName();
        captureScreenshot(methodName);
    }

    protected void captureScreenshot(String name) {
        falconSpoonRule.screenshot(getCurrentActivity(), name);
    }


    public Activity getCurrentActivity() {
        getInstrumentation().waitForIdleSync();
        final Activity[] activity = new Activity[1];
        getInstrumentation().runOnMainSync(() -> {
            java.util.Collection<Activity> activities = ActivityLifecycleMonitorRegistry.getInstance().getActivitiesInStage(Stage.RESUMED);
            activity[0] = Iterables.getOnlyElement(activities);
        });

        return activity[0];
    }

}
