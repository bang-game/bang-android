package ru.aalab.banggame.game.event;

public interface BangListener {
    void bangPlayerOne(BangEvent bangEvent);

    void bangPlayerTwo(BangEvent bangEvent);
}
