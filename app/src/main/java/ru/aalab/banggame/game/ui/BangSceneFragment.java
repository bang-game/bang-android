package ru.aalab.banggame.game.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.RawRes;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lombok.AccessLevel;
import lombok.Setter;
import ru.aalab.banggame.R;
import ru.aalab.banggame.game.event.BangEvent;
import ru.aalab.banggame.game.event.BangListener;
import ru.aalab.banggame.game.scene.Scene;
import ru.aalab.banggame.game.scene.SceneScript;


/**
 * Этот класс реализует игровую сцену от состояния "ready" до смерти одного из игроков
 */
public class BangSceneFragment extends Fragment implements Scene {

    public static final int MESSAGE_DURATION = 1000;
    @BindView(R.id.fragment_bang_scene_player_1_image)
    AppCompatImageView playerOneImage;

    @BindView(R.id.fragment_bang_scene_player_2_image)
    AppCompatImageView playerTwoImage;


    @BindView(R.id.fragment_bang_scene_message)
    AppCompatTextView messageView;

    private Set<BangListener> bangListeners = new HashSet<>();


    private ScheduledExecutorService worldExecutor = Executors.newSingleThreadScheduledExecutor();
    @Setter(value = AccessLevel.PRIVATE)
    private SceneScript sceneScript;


    public static BangSceneFragment newScene(SceneScript sceneScript, BangListener... bangListeners) {
        BangSceneFragment bangSceneFragment = new BangSceneFragment();
        Arrays.asList(bangListeners).forEach(bangSceneFragment::addBangListener);
        bangSceneFragment.setSceneScript(sceneScript);
        return bangSceneFragment;
    }

    private void addBangListener(BangListener bangListener) {
        bangListeners.add(bangListener);
    }


    @Override
    public void onStart() {
        super.onStart();
        if (null == sceneScript) {
            throw new IllegalStateException("Scene no script");
        }

        sceneScript.init(worldExecutor, this);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bang_scene, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @OnClick(R.id.fragment_bang_scene_player_1_bg)
    public void onPlayerOneBgClick() {
        bangListeners.forEach((listener) -> listener.bangPlayerOne(new BangEvent()));
    }

    @OnClick(R.id.fragment_bang_scene_player_2_bg)
    public void onPlayerTwoBgClick() {
        bangListeners.forEach((listener) -> listener.bangPlayerOne(new BangEvent()));
    }


    //-----------------------------------scene-----------------------------------------
    @Override
    public void readyMessage() {
        showMessage(R.string.msg_ready, R.raw.amsg_ready);
    }


    @Override
    public void steadyMessage() {
        showMessage(R.string.msg_steady, R.raw.amsg_steady);
    }


    @Override
    public void bangMessage() {
        showMessage(R.string.msg_bang, R.raw.amsg_bang);
    }

    private void showMessage(@StringRes int textMessage, @RawRes int audioMessage) {
        String messageStr = getString(textMessage);
        final MediaPlayer mp = MediaPlayer.create(getContext(), audioMessage);
        mp.setOnCompletionListener((e) -> mp.release());
        mp.start();


        getActivity().runOnUiThread(() -> {
            messageView.setText(messageStr);
            messageView.setAlpha(1);
            messageView.setVisibility(View.VISIBLE);
            messageView.animate().alpha(0.0f).setDuration(MESSAGE_DURATION).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    messageView.setVisibility(View.INVISIBLE);
                }
            });
        });
    }

    private void runOnUIThread(Runnable runnable) {
        getActivity().runOnUiThread(runnable);
    }
}
