package ru.aalab.banggame.game.scene;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

public class SimpleSceneScript implements SceneScript {


    @Override
    public void afterReady(ScheduledExecutorService executor, Scene scene) {
        final int delay = SceneScript.randomDelay();
        executor.schedule(scene::bangMessage,delay , TimeUnit.SECONDS);
    }


}
