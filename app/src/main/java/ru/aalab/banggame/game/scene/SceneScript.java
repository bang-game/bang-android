package ru.aalab.banggame.game.scene;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

public interface SceneScript {

    int DEFAULT_READY_DELAY = 2;

    int DEFAULT_STEADY_DELAY = 4;

    int DEFAULT_DELAY = DEFAULT_READY_DELAY + DEFAULT_STEADY_DELAY;


    default void init(ScheduledExecutorService executor, Scene scene) {
        executor.schedule(scene::readyMessage, DEFAULT_READY_DELAY, TimeUnit.SECONDS);

        executor.schedule(scene::steadyMessage, DEFAULT_STEADY_DELAY, TimeUnit.SECONDS);

        afterReady(executor, scene);
    }

    void afterReady(ScheduledExecutorService executor, Scene scene);


    static int randomDelay() {
        return ThreadLocalRandom.current().nextInt(1, 5) + DEFAULT_DELAY;
    }

}
