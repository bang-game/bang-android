package ru.aalab.banggame.game.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.aalab.banggame.R;
import ru.aalab.banggame.game.scene.SimpleSceneScript;

/**
 * Activity для управления игровой сценой
 */
public class BangSceneActivity extends AppCompatActivity {

    @BindView(R.id.activity_bang_scene_bottom_info_frame)
    View infoFrame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bang_scene);
        ButterKnife.bind(this);
        setFragment(BangSceneFragment.newScene(new SimpleSceneScript()));
    }


    protected void setFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.activity_bang_scene_frame, fragment);
        fragmentTransaction.commit();
    }
}
